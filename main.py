import pygame, os
from pygame.locals import *

ancho = 640
alto = 480

class sprite(pygame.sprite.Sprite):
  def __init__(self, nombre, posx, posy, vel):
    self.imagen = pygame.image.load(os.path.join("./imagenes", nombre))
    self.rect = self.imagen.get_rect()
    self.rect.centerx = int(posx)
    self.rect.centery = int(posy)
    self.velocidad = [vel, -vel]

  def mover(self, tiempo, key):
    if key[K_UP]:
      self.rect.y -= int(self.velocidad[0] * tiempo)
      if self.rect.y <= 0:
        self.rect.y = 0
    if key[K_DOWN]:
      self.rect.centery += int(self.velocidad[0] * tiempo)
      if self.rect.bottom >= 480:
        self.rect.bottom = 480

  def ia(self, tiempo, ball):
    if ball.velocidad[0] >= 0 and ball.rect.x >= ancho/2:
      if self.rect.centery < ball.rect.centery:
        self.rect.centery += int(self.velocidad[0] * tiempo)
      if self.rect.centery > ball.rect.centery:
        self.rect.centery -= int(self.velocidad[0] * tiempo)
      if self.rect.top <= 0:
        self.rect.top = 0
      if self.rect.bottom >= 640:
        self.rect.bottom = 640
        
  def mover_pelota(self, tiempo, paleta1, paleta2, puntos = {}):
    self.rect.centerx += int(self.velocidad[0] * tiempo)
    self.rect.centery += int(self.velocidad[1] * tiempo)

    if pygame.sprite.collide_rect(self, paleta1):
      self.velocidad[0] = -self.velocidad[0]
      self.rect.centerx += int(self.velocidad[0] * tiempo)
    if pygame.sprite.collide_rect(self, paleta2):
      self.velocidad[0] = -self.velocidad[0]
      self.rect.centerx += int(self.velocidad[0] * tiempo)

    if self.rect.left <= 0:
      puntos[1] += 1
    if self.rect.right >= 640:
      puntos[0] += 1

    if self.rect.top <= 0:
      self.velocidad[1] = -self.velocidad[1]
      self.rect.centery += int(self.velocidad[1] * tiempo)
    if self.rect.bottom >= 480:
      self.velocidad[1] = -self.velocidad[1]
      self.rect.centery += int(self.velocidad[1] * tiempo)

def reinicio(bola):
  if bola.rect.x <= 0:
    bola.rect.x = int(ancho/2)
    bola.velocidad[0] = -bola.velocidad[0]
  if bola.rect.right >= 640:
    bola.rect.x = int(ancho/2)
    bola.velocidad[0] = -bola.velocidad[0]

def p_texto(texto, posx, posy, color = (255, 255, 255)):
  fuente = pygame.font.SysFont("Arial", 20)
  t_salida = fuente.render(texto, True, color)
  #
  t_salida_rect = t_salida.get_rect()
  t_salida_rect.x = posx
  t_salida_rect.y = posy
  return t_salida, t_salida_rect
  

def main():
  pygame.init()
  icono = pygame.image.load(os.path.join("./imagenes", "pokebola.png"))
  pygame.display.set_icon(icono)
  vent = pygame.display.set_mode([ancho,alto])
  pygame.display.set_caption("Pong!!!")
  reloj = pygame.time.Clock()
  #
  pala1 = sprite("paleta1.png", ancho - 590, (alto/2), 0.5)
  pala2 = sprite("paleta2.png", ancho - 50, (alto/2), 0.5)
  pelota = sprite("bolita.png", ancho/2, alto/2, 0.1)
  fondo = pygame.image.load(os.path.join("./imagenes", "fondo.png"))
  #
  verde = (34, 177, 76)
  blanco = (255, 255, 255)
  rect = pygame.Rect(0, 0, 200, 100)
  #
  puntos = [0, 0]
  salir = False
  #
  while salir != True:
    reloj.tick(60)
    reinicio(pelota)
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        salir = True

    vent.blit(fondo, [0,0])
    #
    pala1.mover(reloj.get_fps(), pygame.key.get_pressed())
    pelota.mover_pelota(reloj.get_fps(), pala1, pala2, puntos)
    pala2.ia(reloj.get_fps(), pelota)
    #
    pygame.draw.rect(vent, verde, rect)
    pygame.draw.line(vent, blanco, (0, 50), (200, 50))
    pygame.draw.line(vent, blanco, (160, 0), (160, 100))
    #
    p_jug, p_jug_rect = p_texto("Jugador 1 ", 10, 15)
    p_cpu, p_cpu_rect = p_texto("CPU ", 10, 65)
    vent.blit(p_jug, p_jug_rect)
    vent.blit(p_cpu, p_cpu_rect)
    #
    p_jug_text, p_jug_text_rect = p_texto(str(puntos[0]), 180, 15)
    p_cpu_text, p_cpu_text_rect = p_texto(str(puntos[1]), 180, 65)
    vent.blit(p_jug_text, p_jug_text_rect)
    vent.blit(p_cpu_text, p_cpu_text_rect)
    #
    vent.blit(pala1.imagen, pala1.rect)
    vent.blit(pala2.imagen, pala2.rect)
    vent.blit(pelota.imagen, pelota.rect)
    
    pygame.display.update()
  pygame.quit()

main()
